'use strict';

angular.module('employeeApp')
    .directive("employeeList", function() {
    return {
        restrict : "E",
        scope : {
            employeeData : "=employeeData",
        },
        templateUrl : '../../views/employee-list.html'
    };
});