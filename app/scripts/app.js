'use strict';

/**
 * @ngdoc overview
 * @name employeeApp
 * @description
 * # employeeApp
 *
 * Main module of the application.
 */
angular
  .module('employeeApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('employee', {
        abstract: true,
        template: '<nav ui-view="header" class="navbar navbar-inverse navbar-fixed-top"></nav>'+
                      '<div class="container-fluid"> <div class="row"> <div ui-view="left-sidebar" class="col-sm-3 col-md-2 sidebar"></div>'+
                      '<div ui-view="employees" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"></div> </div>'+
                      '<div ui-view="footer"></div> </div>',
    })
    .state('employee.physical',{
      url:'/employees',
      views: {
        'header':{
            templateUrl : 'views/header.html'
        },
        'left-sidebar' : {
            templateUrl : 'views/left-sidebar.html',
        },
        'employees' : {
            templateUrl : 'views/employees.html',
            controller : 'EmployeeController'
        },
        'footer' : {
            templateUrl : 'views/footer.html'
        }
      }
    });
    $urlRouterProvider.otherwise('/employees');
  }).run(function(){
    return true;
  });
