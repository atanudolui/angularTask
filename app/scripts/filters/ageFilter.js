'use strict';
angular.module('employeeApp')
    .filter('ageFilter', function() {
    function calculateAge(birthday) {
        birthday=new Date(parseInt(birthday)*1000);
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    return function(birthdate) {
        return calculateAge(birthdate);
    };
});